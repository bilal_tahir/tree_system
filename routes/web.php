<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('website.home');
// });

Route::get('/', 'WebsiteContent@home');
Route::get('/about', 'WebsiteContent@about');
Route::get('/how', 'WebsiteContent@how');
Route::get('/privacypolicy', 'WebsiteContent@privacy');
Route::get('/refundpolicy', 'WebsiteContent@refund');
Route::get('/contactus', 'WebsiteContent@contact');
Route::post('/contact', 'AdminController@submitcontact');

// Route::get('/about', function () {
//     return view('website.about');
// });
// Route::get('/how', function () {
//     return view('website.packages');
// });
Route::get('/user', function () {
    return view('main');
})->middleware('auth');








Route::post('/registered', 'HomeController@logic');
Route::get('/products', 'ProductsController@index');




//Route for category pages
Route::get('toyota','ProductsController@toyota');
Route::get('honda','ProductsController@honda');
Route::get('suzuki','ProductsController@suzuki');
Route::get('hyundai','ProductsController@hyundai');
Route::get('nissan','ProductsController@nissan');

//Route for filters
Route::get('keyword','ProductsController@keyword');
Route::get('price1','ProductsController@price1');
Route::get('price2','ProductsController@price2');
Route::get('price3','ProductsController@price3');
Route::get('price4','ProductsController@price4');
Route::get('price5','ProductsController@price5');






Route::get('cart/{cart}', ['as' => 'cart_items','uses' =>'ProductsController@cart']);



// Route::get('carts','ProductsController@showcart');


Route::get('cartsdelete/{cartsdelete}', ['as' => 'cartdelete','uses' =>'ProductsController@cartdelete']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth' , 'admin']], function (){
 Route::get('/admin', 'AdminController@index');
 Route::get('/basic', 'AdminController@index1');
 Route::get('/standard', 'AdminController@index2');
 Route::get('/premium', 'AdminController@index3');
 Route::get('/adminproducts', 'AdminController@index4');
 Route::get('/productdelete/{id}', 'AdminController@index5');
 Route::get('/add','AdminController@index6');
 Route::get('/admin/home','AdminController@home');
 Route::get('/admin/about','AdminController@about');
 Route::get('/admin/how','AdminController@how');
 Route::get('/admin/privacy','AdminController@privacy');
 Route::get('/admin/refund','AdminController@refund');
 Route::get('/admin/contact','AdminController@contact');
Route::get(' /admin/updatecontent/{id}', 'AdminController@updatecontent');
Route::post('/update/{id}', 'AdminController@update');
 Route::get('/admin/message','AdminController@message');
Route::get('/delete/{id}', 'AdminController@deletemessage');
 Route::get('/admin/footer','AdminController@footer');
 Route::get('/update/footer/{id}', 'AdminController@updatefooter');
 Route::post('/footerupdated/{id}', 'AdminController@footerupdated');
  Route::get('/admin/paymentimages','AdminController@paymentimages');
   Route::get('/update/paymentimages/{id}', 'AdminController@updatepaymentimages');
   Route::post('/paymentimageupdated/{id}', 'AdminController@paymentimageupdated');
    Route::get('/add/paymentimage','AdminController@addpaymentimages');
       Route::post('/paymentimageadded', 'AdminController@paymentimageadded');
    // Route::get('/delete/paymentimage','AdminController@deletepaymentimages');
     Route::get('/imagedelete','AdminController@deleteimage');
    Route::get('/imagedeleted/{id}', 'AdminController@imagedeleted');
    
   



 Route::post('/addproduct', 'AdminController@index7')->name('upload');





 Route::get('/productupdate/{id}', 'AdminController@index8');
Route::post('/updateproduct/{id}', 'AdminController@index9');

});
