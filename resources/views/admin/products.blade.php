@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')


 <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Products </h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Id</th>
                     
                      <th>Name</th>
                      <th>Quantity</th>
                      <th>Price</th>
                       <th>Image</th>
                      <th>Category</th>
                      <th>SubCategory</th>
                      <th>Update</th>
                       <th>Delete</th>
                      
                    </thead>
                    <tbody>
                     @foreach($userdata2 as $data)
                     <tr>
                        <td>{{$data->id}}</td>
                   
                        <td>{{$data->name}}</td>
                        <td>{{$data->quantity}}</td>
                        <td>{{$data->price}}</td>
                        <td style="width: 30%"><img class="img-profile" src="/storage/images/{{$data->image}}" style="width: 50%"></td>
                        <td>{{$data->category}}</td>
                        <td>{{$data->subcategory}}</td>
                        <td> <a class="btn btn-primary btn-sm" href="{{ url('/productupdate',$data->id) }}">Edit</a></td>
                        <td><a class="btn btn-danger btn-sm" href="{{ url('/productdelete',$data->id) }}">Delete</a></td>
                      
                      </tr> 
                     @endforeach
                    </tbody>
                  </table>
                   <!-- <a href="/addproducts"><button type="submit" class="btn btn-success">Add a Product</button></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
