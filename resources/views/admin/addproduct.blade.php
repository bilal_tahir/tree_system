@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')

<form method="POST" action="{{route('upload') }}" enctype="multipart/form-data">
@csrf
  <meta name="csrf-token" content="{{ csrf_token() }}" >
  <div class="form-group">
    <label for="exampleInputEmail1">Product Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Enter name">
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Product Quantity</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="quantity" aria-describedby="emailHelp" placeholder="Enter quantity">
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Product Price</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="price" aria-describedby="emailHelp" placeholder="Enter price">
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Product Category</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="category" aria-describedby="emailHelp" placeholder="Enter category">
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Product Subcategory</label>
    <input type="text" class="form-control" id="exampleInputEmail1" name="subcategory" aria-describedby="emailHelp" placeholder="Enter subcategory">
  </div>
    <div class="form-group">
     <label for="exampleFormControlFile1">Product Image</label>
    <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
  </div>

  <button type="submit" class="btn btn-primary">Add</button>
</form>

@endsection