@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')

       <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> 30$ Package Customer </h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Id</th>
                      <th>Refferal_Id</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Number</th>
                       <th>Country</th>
                      <th>Package</th>
                      <th>Bank</th>
                      <th>Account_Name</th>
                       <th>Account_Number</th>
                      <th>Password</th>
                      <th>IP_Address</th>
                      <th>Total_Balance</th>
                    </thead>
                    <tbody>
                     @foreach($userdata2 as $data)
                     <tr>
                        <td>{{$data->id}}</td>
                        <td>{{$data->p_id}}</td>
                        <td>{{$data->name}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->number}}</td>
                        <td>{{$data->country}}</td>
                        <td>{{$data->package}}</td>
                        <td>{{$data->bank}}</td>
                        <td>{{$data->account_name}}</td>
                        <td>{{$data->account_number}}</td>
                        <td>{{$data->password}}</td>
                        <td>{{$data->ip_address}}</td>
                        <td>{{$data->reward}}</td>
                      </tr> 
                     @endforeach
                    </tbody>
                  </table>
                   <!-- <a href="/addproducts"><button type="submit" class="btn btn-success">Add a Product</button></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection