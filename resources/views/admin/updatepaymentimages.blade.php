
                                    @extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')

<form  action="{{url('/paymentimageupdated', $payimage->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                            @csrf
                                            <meta name="csrf-token" content="{{ csrf_token() }}">

                                              <div class="row form-group">
                                                <div class="col col-md-3">
                                                  <label>Update Payment Image</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="file" id="image" name="image" class="form-control-file">
                                                </div>
                                            </div>

                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-dot-circle-o"></i> Update
                                                </button>             
                                            </div>
                                    </form>

@endsection

