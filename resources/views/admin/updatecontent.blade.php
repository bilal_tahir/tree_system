@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')

<form method="POST" action="{{url('/update', $update->id) }}" enctype="multipart/form-data">
@csrf
  <meta name="csrf-token" content="{{ csrf_token() }}" >
  <div class="form-group">
    <label>Add New Heading</label>
    <input type="text" class="form-control"  name="heading"  >
  </div>
    <div class="form-group">
    <label>Add New Content</label>
     <textarea name="content" id="summernote" rows="9" placeholder="Content..." class="form-control"></textarea>
                                                
  </div>

 
   
 
   

  <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('#summernote').summernote();
});
  </script>
@endsection