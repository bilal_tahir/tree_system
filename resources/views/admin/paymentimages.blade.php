@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')


 <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Update Payment Images </h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                    
                      <th>Image</th>
                     <th>Action</th>
                      
                    </thead>
                    <tbody>
                     @foreach($images as $data)
                     <tr>
                        
                        <td><img src="{{ asset('assetsss/images') . '/'.$data->image}}" class="set"></td>
                        
                        <td><a class="btn btn-primary btn-sm" href="{{ url('/update/paymentimages',$data->id) }}">Update</a></td>
                      
                      </tr> 
                     @endforeach
                    </tbody>
                  </table>
                   <!-- <a href="/addproducts"><button type="submit" class="btn btn-success">Add a Product</button></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
