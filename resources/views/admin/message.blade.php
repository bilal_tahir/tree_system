@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')


 <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Update Home Content </h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Name</th> 
                      <th>Email</th>
                      <th>Phone Number</th>
                      <th>Message</th>
                     <th>Action</th>
                      
                    </thead>
                    <tbody>
                     @foreach($messages as $data)
                     <tr>
                        <td>{{$data->name}}</td>
                        <td>{{$data->email}}</td>
                          <td>{{$data->phone}}</td>
                        <td>{{$data->message}}</td>
                        <td><a class="btn btn-primary btn-sm" href="{{ url('/delete',$data->id) }}">Delete</a></td>
                      
                      </tr> 
                     @endforeach
                    </tbody>
                  </table>
                   <!-- <a href="/addproducts"><button type="submit" class="btn btn-success">Add a Product</button></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
