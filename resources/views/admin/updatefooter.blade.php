@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')

<form method="POST" action="{{url('/footerupdated', $updatefoot->id) }}" enctype="multipart/form-data">
@csrf
  <meta name="csrf-token" content="{{ csrf_token() }}" >
  <div class="form-group">
    <label>Add New Heading</label>
    <input type="text" class="form-control"  name="heading"  >
  </div>
    <div class="form-group">
    <label>Add New Content</label>
     <input type="text" class="form-control"  name="text"  >
                                                
  </div>

 
   
 
   

  <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection

