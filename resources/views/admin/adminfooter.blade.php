@extends('admin_layout.master')

@section('title')

Admin Dashboard
@endsection
@section('content')


 <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Update Footer Content </h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Heading</th> 
                      <th>Text</th>
                     <th>Action</th>
                      
                    </thead>
                    <tbody>
                     @foreach($footercon as $data)
                     <tr>
                        <td>{{$data->heading}}</td>
                        <td>{{$data->text}}</td>
                        
                        <td><a class="btn btn-primary btn-sm" href="{{ url('/update/footer',$data->id) }}">Update</a></td>
                      
                      </tr> 
                     @endforeach
                    </tbody>
                  </table>
                   <!-- <a href="/addproducts"><button type="submit" class="btn btn-success">Add a Product</button></a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
