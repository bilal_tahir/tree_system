@extends('ecommerce_layout.main')
@section('title')
Home
@endsection
@section('content')
 <div class="homeslider slides">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      

        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100" src="{{asset('assetsss/images/sliders.jpg')}}" alt="First slide"><!-- d-none this is if mobile view dont have text on slider-->
           <div class="carousel-content d-md-block">
                    <h5>Welcome to Get29</h5>
                    <p>A secure place to achieve financial freedom and security. Welcome to Getters Network.</p>
                </div>
            </div>
            
           
        </div>
       
    </div>
</div>


    {{-- <div class="slant-1"></div> --}}

    
    
  <div class="site-half first-section">

    <div class="container">
      <div class="row no-gutters align-items-stretch">
        <div class="col-lg-12 ml-lg-auto py-5">
          
        <h2 class="site-section-heading text-uppercase font-secondary  cus">{{$howcontent[0]->heading}}</h2>
 {!!html_entity_decode($howcontent[0]->text)!!}  </div>
      </div>
    </div>
  </div>

   <div class="site-half block">

    <div class="container">
      <div class="row no-gutters align-items-stretch">
        <div class="col-lg-12 ml-lg-auto py-5">
           <img class="logo_setting" src="{{asset('assetsss/images/child.jpg')}}"    alt="First slide">
   </div>
      </div>
    </div>
  </div>

@endsection