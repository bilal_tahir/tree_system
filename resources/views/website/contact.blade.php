@extends('ecommerce_layout.main')
@section('title')
Home
@endsection
@section('content')
 <div class="homeslider slides">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      

        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100" src="{{asset('assetsss/images/sliders.jpg')}}" alt="First slide"><!-- d-none this is if mobile view dont have text on slider-->
           <div class="carousel-content d-md-block">
                    <h5>Welcome to Get29</h5>
                    <p>A secure place to achieve financial freedom and security. Welcome to Getters Network.</p>
                </div>
            </div>
            
           
        </div>
       
    </div>
</div> 

    {{-- <div class="slant-1"></div> --}}

    
    
  <div class="site-section first-section" data-aos="fade">
      <div class="container">
        @if ($message = Session::get('success'))
 
                <div class="alert alert-success alert-block">
 
                    <button type="button" class="close" data-dismiss="alert">×</button>
 
                    <strong>{{ $message }}</strong>
 
                </div>
            @endif
        <div class="row">
       
          <div class="col-md-12 col-lg-8 mb-5">
          
            
          
            <form action="/contact" method="POST" class="p-5 bg-white">
              @csrf
              <meta name="csrf-token" content="{{ csrf_token() }}">

              <div class="row form-group">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="fullname">Full Name</label>
                  <input type="text" id="fullname" name="fullname" class="form-control" placeholder="Full Name">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="email">Email</label>
                  <input type="email" id="email" name="email" class="form-control" placeholder="Email Address">
                </div>
              </div>


              <div class="row form-group">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="phone">Phone</label>
                  <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone #">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="message">Message</label> 
                  <textarea name="message" id="message" name="message" cols="30" rows="5" class="form-control" placeholder="Say hello to us"></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-warning text-white px-4 py-2">
                </div>
              </div>

  
            </form>
          </div>

          <div class="col-lg-4">
            <div class="p-4 mb-3 bg-white">
              <h3 class="h5 text-black mb-3">Contact Info</h3>
            <p class=" font-weight-bold cuus">{{$contactcontent[0]->heading}}</p>
             {!!html_entity_decode($contactcontent[0]->text)!!}

              <p class=" font-weight-bold cuus">{{$contactcontent[1]->heading}}</p>
                {!!html_entity_decode($contactcontent[1]->text)!!}

              <p class=" font-weight-bold cuus">{{$contactcontent[2]->heading}}</p>
              {!!html_entity_decode($contactcontent[2]->text)!!}

            </div>
            
            
          </div>
        </div>

        
      </div>
    </div>

 
@endsection