@extends('ecommerce_layout.main')
@section('title')
Home
@endsection
@section('content')

    <div class="homeslider slides">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      

        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100" src="{{asset('assetsss/images/sliders.jpg')}}" alt="First slide"><!-- d-none this is if mobile view dont have text on slider-->
           <div class="carousel-content d-md-block">
                    <h5>Welcome to Get29</h5>
                    <p>A secure place to achieve financial freedom and security. Welcome to Getters Network.</p>
                </div>
            </div> 
        </div>
    </div>
</div>

   

    <div class="site-section first-section">
      <div class="container">
        <!-- <div class="row mb-5">
          <div class="col-md-12 text-center" data-aos="fade"> 
            <span class="caption d-block mb-2 font-secondary font-weight-bold">What is Get29 all about?</span>
            <p class="site-section-heading  text-center font-secondary">A secure place to achieve financial freedom and security. <br> Welcome to Getters Network.</p>
          </div>
        </div> -->


        <div class="row border-responsive cus">
          <div class="col-md-4 col-lg-4 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="">
            <div class="text-center">
              <span class="flaticon-money-bag-with-dollar-symbol display-4 d-block mb-3 flc"></span>
            <h3 class="text-uppercase h4 mb-3">{{$homecontent[0]->heading}}</h3>
              {!!html_entity_decode($homecontent[0]->text)!!}
            </div>
          </div>
            <div class="col-md-4 col-lg-4 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="">
            <div class="text-center">
              <span class="flaticon-money-bag-with-dollar-symbol display-4 d-block mb-3 flc"></span>
             <h3 class="text-uppercase h4 mb-3">{{$homecontent[1]->heading}}</h3>
              {!!html_entity_decode($homecontent[1]->text)!!}
            </div>
          </div>
            <div class="col-md-4 col-lg-4 mb-4 mb-lg-0 border-right" data-aos="fade-up" data-aos-delay="">
            <div class="text-center">
              <span class="flaticon-money-bag-with-dollar-symbol display-4 d-block mb-3 flc"></span>
             <h3 class="text-uppercase h4 mb-3">{{$homecontent[2]->heading}}</h3>
              {!!html_entity_decode($homecontent[2]->text)!!}
            </div>
          </div>
        
         
        </div>
      </div>
    </div>



<div class="container">
    <div class="row">
  <div class="row">
    @foreach($paymentimages as $imagedata)
    <div class="col-4">
     <img class="logo_setting" src="{{ asset('assetsss/images') . '/'.$imagedata->image}}"  style="width:100%; height:100%; margin-bottom:15px;"  alt="First slide">
</div>
@endforeach
 
</div>
</div>
</div>



    </div>


@endsection