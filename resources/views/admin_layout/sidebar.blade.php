<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Get <sup>29</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}" target="_blank">
            <i class="fas fa-fw fa-globe"></i>
            <span>Website</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-microphone"></i>
            <span>Customer Details</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ url('/basic') }}">3$ Package Customer</a>
                <a class="collapse-item" href="{{ url('/standard') }}">6$ Package Customer</a>
                <a class="collapse-item" href="{{ url('/premium') }}">30$ Package Customer</a>
            </div>
        </div>
    </li>


      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseContent" aria-expanded="true" aria-controls="collapseContent">
            <i class="fas fa-fw fa-book"></i>
            <span>Website Content</span>
        </a>
        <div id="collapseContent" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ url('/admin/home') }}">Home</a>
                <a class="collapse-item" href="{{ url('/admin/about') }}">About</a>
                <a class="collapse-item" href="{{ url('/admin/how') }}">How It Work</a>
                <a class="collapse-item" href="{{ url('/admin/privacy') }}">Privacy Policy</a>
                <a class="collapse-item" href="{{ url('/admin/refund') }}">Refund Policy </a>
                <a class="collapse-item" href="{{ url('/admin/contact') }}">Contact Info </a>
                 <a class="collapse-item" href="{{ url('/admin/footer') }}">Footer Content </a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-camera"></i>
            <span>Products</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
             <a class="collapse-item" href="{{ url('/adminproducts') }}">All Products</a>
                <a class="collapse-item" href="{{ url('/add') }}">Add New Product</a>
            </div>
        </div>
    </li>

     <!-- Nav Payment images - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePaymentImages" aria-expanded="true" aria-controls="collapsePaymentImages">
            <i class="fas fa-fw fa-image"></i>
            <span>Payment Images</span>
        </a>
        <div id="collapsePaymentImages" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ url('/admin/paymentimages') }}">Update Payment Images</a>
                <a class="collapse-item" href="{{ url('/add/paymentimage') }}">Add New Payment Image</a>
                  <a class="collapse-item" href="{{ url('/imagedelete') }}">Delete Payment Image</a>
                
            </div>
        </div>
    </li>

    <!-- Home Menu Dashboard -->
     <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#contact" aria-expanded="true" aria-controls="contact">
            <i class="fas fa-fw fa-inbox"></i>
            <span>Users Messages</span>
        </a>
        <div id="contact" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
             <a class="collapse-item" href="{{ url('/admin/message') }}">All Messages</a>
            </div>
        </div>
    </li>



    <!-- Admin Registeration 
    <li>
  <h2><a href = "/register">register</a></h2>
    </li>
-->
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
