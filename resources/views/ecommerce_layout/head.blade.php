<head>
   <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=Oswald:400,700|Work+Sans:300,400,700" rel="stylesheet">



<link rel="stylesheet" type="text/css" href="{{asset('assetsss/fonts/icomoon/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/magnific-popup.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/owl.carousel.min.cs')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/fonts/flaticon/font/flaticon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/aos.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assetsss/css/style.css')}}">





<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    

	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/fonts/themify/themify-icons.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/fonts/elegant-font/html-css/style.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/slick/slick.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/vendor/lightbox2/css/lightbox.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assetssss/css/main.css')}}">
<!--===============================================================================================-->

  </head>