<!doctype html>
<html lang="en">

@include('ecommerce_layout.head')
<body>

    @include('ecommerce_layout.header')

    @yield('content')

    @include('ecommerce_layout.footer')
    <script type="text/javascript" src="{{asset('assetsss/js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/popper.min.jss')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.stellar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.animateNumber.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/aos.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/main.js')}}"></script>
<!-- ecommerce for javascript -->

<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/bootstrap/js/popper.js')}}"></script>
	<script type="text/javascript" src="{{asset('assetssss/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/slick/slick.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assetssss/js/slick-custom.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/lightbox2/js/lightbox.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{asset('assetssss/vendor/sweetalert/sweetalert.min.js')}}"></script>
	

<!--===============================================================================================-->
	<script src="{{asset('assetssss/js/main.js')}}"></script>

     <!-- /theme js plugins  -->
     @yield('page-level-js-plugin')

@yield('page-level-js')

  </body>
</html>