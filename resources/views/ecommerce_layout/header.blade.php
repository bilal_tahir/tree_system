 {{-- <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-warning"  role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div> --}}

  <div class="site-wrap">

    

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    
    <div class="site-navbar-wrap js-site-navbar bg-white">
      
      <div class="container">
        <div class="site-navbar ">
          <div class="row align-items-center">
          <div class="col-2">
              <h2 class=" site-logo"><a href="/" > <img src="{{asset('assets/img/logo.jpg')}}" alt="First slide"></a></h2>
            </div>
            <div class="col-10">
              <nav class="site-navigation text-right" role="navigation">
                <div class="container">
                               <a href="{{ route('login') }}"><button type="button" class="btn btn_cus cus1  d-lg-none d-xl-none">Login</button></a>
                    <a href="{{ route('register') }}"> <button type="button" class="btn btn_cus cus2 d-lg-none d-xl-none">Register</button></a>
                 
                  <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                  <ul class="site-menu js-clone-nav d-none d-lg-block">
                    <li class=""><a href="{{url('/')}}">Home</a></li>
                   <li><a href="{{url('/about')}}">About Us</a></li>
                   <li><a href="{{url('/products')}}">Products</a></li>
                    <li><a href="{{url('/how')}}">How It Works</a></li>
                    <li><a href="{{url('/privacypolicy')}}">Privacy Policy</a></li>
                    <li><a href="{{url('/refundpolicy')}}">Refund Policy</a></li>
                    <li><a href="{{url('/contactus')}}">Contact Us</a></li>
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  