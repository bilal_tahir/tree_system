@extends('layouts.app')

@section('content')
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background-color:white; opacity:0.9;" >
                <div class="card-header" >{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
<!-- Sponsor Id -->
 <!-- <div class="form-group row">
                            <label for="sponsorid" class="col-md-4 col-form-label text-md-right">{{ __('Sponsor Id') }}</label>

                            <div class="col-md-6">
                                <input id="sponsorid" type="id" class="form-control @error('sponsorid') is-invalid @enderror" name="sponsorid" value="{{ old('sponsorid') }}" required autocomplete="sponsorid" autofocus>

                                @error('sponsorid')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
<!-- Name -->
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
<!-- Email -->
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
<!-- Number -->
                        <div class="form-group row">
                            <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-6">
                                <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ old('number') }}" required autocomplete="number" autofocus>

                                @error('number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
<!-- Country Name -->
  {{-- <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" required autocomplete="country" autofocus>

                                @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  --}}
<!-- Package -->
 <!-- <div class="form-group row">
                            <label for="package" class="col-md-4 col-form-label text-md-right">{{ __('Package') }}</label>

                            <div class="col-md-6">
                                <select id="package" type="text" class="form-control @error('package') is-invalid @enderror" name="package" value="{{ old('package') }}" required autocomplete="package" autofocus>
                            <option  style="color: black" value="Basic 3$ Package">Basic 3$ Package</option>
              				<option  style="color: black" value="Standard 6$ Package">Standard 6$ Package</option>
              				<option  style="color: black" value="Premium 30$ Package">Premium 30$ Package</option>    
                    </select>
                                @error('package')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
                        <!-- Bank-->
        <div class="form-group row">
            <label for="bankname" class="col-md-4 col-form-label text-md-right">{{ __('Bank Name') }}</label>

            <div class="col-md-6">
                <input id="bankname" type="text" class="form-control @error('bankname') is-invalid @enderror" name="bankname" value="{{ old('bankname') }}" required autocomplete="bankname" autofocus>

                @error('bankname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

<!--Account Name -->
{{-- <div class="form-group row">
                            <label for="bankaccountname" class="col-md-4 col-form-label text-md-right">{{ __('Account Name') }}</label>

                            <div class="col-md-6">
                                <input id="bankaccountname" type="text" class="form-control @error('bankaccountname') is-invalid @enderror" name="bankaccountname" value="{{ old('bankaccountname') }}" required autocomplete="bankaccountname" autofocus>

                                @error('bankaccountname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> --}}
<!-- Account Number -->
<div class="form-group row">
                            <label for="bankaccountnumber" class="col-md-4 col-form-label text-md-right">{{ __('Account Number') }}</label>

                            <div class="col-md-6">
                                <input id="bankaccountnumber" type="text" class="form-control @error('bankaccountnumber') is-invalid @enderror" name="bankaccountnumber" value="{{ old('bankaccountnumber') }}" required autocomplete="bankaccountnumber" autofocus>

                                @error('bankaccountnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <!-- Password -->
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
