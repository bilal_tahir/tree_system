<!doctype html>
<html lang="en">

@include('website_layout.head')
<body>

    @include('website_layout.header')

    @yield('content')

    @include('website_layout.footer')

<script type="text/javascript" src="{{asset('assetsss/js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/popper.min.jss')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.stellar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/jquery.animateNumber.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/aos.js')}}"></script>
<script type="text/javascript" src="{{asset('assetsss/js/main.js')}}"></script>


  
  </body>
</html>