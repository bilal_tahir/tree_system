@extends('ecommerce_layout.main')
@section('title')
Home
@endsection
@section('content')
  <div class="homeslider slides">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      

        <div class="carousel-inner">
            <div class="carousel-item active ">
                <img class="d-block w-100" src="{{asset('assetsss/images/sliders.jpg')}}" alt="First slide"><!-- d-none this is if mobile view dont have text on slider-->
           <div class="carousel-content d-md-block">
                    <h5>Welcome to Get29</h5>
                    <p>A secure place to achieve financial freedom and security. Welcome to Getters Network.</p>
                </div>
            </div>
            
           
        </div>
       
    </div>
</div>
<section class="bgwhite  p-b-65">
		<div class="container">
			<div class="row">
            
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
					<div class="leftbar p-r-20 p-r-0-sm">
						<!--  -->
						<h4 class="m-text14 p-b-7">
							Categories
						</h4>

						<ul class="p-b-54">
							<li class="p-t-4">
								<a href="/products" class="s-text13 active1">
									All
								</a>
							</li>

							<li class="p-t-4">
								<a href="/honda" class="s-text13">
									Honda
								</a>
							</li>

							<li class="p-t-4">
								<a href="/toyota" class="s-text13">
									Toyota
								</a>
							</li>

							<li class="p-t-4">
								<a href="/suzuki" class="s-text13">
									Suzuki
								</a>
							</li>

							<li class="p-t-4">
								<a href="/hyundai" class="s-text13">
									Hyundai
								</a>
							</li>

                            
							<li class="p-t-4">
								<a href="/nissan" class="s-text13">
									Nissan
								</a>
							</li>
						</ul>

						<!--  -->
						<h4 class="m-text14 p-b-32">
							Filters
						</h4>
<!-- Prics Filter -->
						<div class="filter-price p-t-22 p-b-50 bo3">
							<div class="m-text15 p-b-17">
								Price
							</div>

							<ul class="p-b-54">
							<li class="p-t-4">
								<a href="/price1" class="s-text13 active1">
								$0.00 - $50.00
								</a>
							</li>


							<li class="p-t-4">
								<a href="/price2" class="s-text13">
									$50.00 - $100.00
								</a>
							</li>

							<li class="p-t-4">
								<a href="/price3" class="s-text13">
									$100.00 - $150.00
								</a>
							</li>

							<li class="p-t-4">
								<a href="/price4" class="s-text13">
									$150.00 - $200.00
								</a>
							</li>

							<li class="p-t-4">
								<a href="/price5" class="s-text13">
									$200.00+
								</a>
							</li>
						</ul>


							
						<!--	<div class="flex-sb-m flex-w p-t-16">
								<div class="w-size11">
									
									<button class="flex-c-m size4 bg7 bo-rad-15 hov1 s-text14 trans-0-4">
										Filter
									</button>
								</div>

								<div class="s-text3 p-t-10 p-b-10">
									Range: $<span id="value-lower">610</span> - $<span id="value-upper">980</span>
								</div>
							</div>
-->
						</div>
<!-- Colors Filters are commented -->
					<!--	<div class="filter-color p-t-22 p-b-50 bo3">
							<div class="m-text15 p-b-12">
								Color
							</div>

							<ul class="flex-w">
								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter1" type="checkbox" name="color-filter1">
									<label class="color-filter color-filter1" for="color-filter1"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter2" type="checkbox" name="color-filter2">
									<label class="color-filter color-filter2" for="color-filter2"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter3" type="checkbox" name="color-filter3">
									<label class="color-filter color-filter3" for="color-filter3"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter4" type="checkbox" name="color-filter4">
									<label class="color-filter color-filter4" for="color-filter4"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter5" type="checkbox" name="color-filter5">
									<label class="color-filter color-filter5" for="color-filter5"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter6" type="checkbox" name="color-filter6">
									<label class="color-filter color-filter6" for="color-filter6"></label>
								</li>

								<li class="m-r-10">
									<input class="checkbox-color-filter" id="color-filter7" type="checkbox" name="color-filter7">
									<label class="color-filter color-filter7" for="color-filter7"></label>
								</li>
							</ul>
						</div>  -->

					<!--	<div class="search-product pos-relative bo4 of-hidden">
							<input class="s-text7 size6 p-l-23 p-r-50" type="text" name="search-product" placeholder="Search Products...">

							<button class="flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
							<a	 href=" "> <i class="fs-12 fa fa-search" aria-hidden="true"></i> </a>
							</button>
						</div>
						-->
					</div>
				</div>

	
					<!--  
					<div class="flex-sb-m flex-w p-b-35">
						<div class="flex-w">
							<div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
								<select class="selection-2" name="sorting">
									<option>Default Sorting</option>
									<option>Popularity</option>
									<option>Price: low to high</option>
									<option>Price: high to low</option>
								</select>
							</div>

							<div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
								<select class="selection-2" name="sorting">
									<option>Price</option>
									<option><a href="/price1">$0.00 - $50.00</a></option> 
									<option>$50.00 - $100.00</option>
									<option>$100.00 - $150.00</option>
									<option>$150.00 - $200.00</option>
									<option>$200.00+</option>

								</select>
							</div>
						</div>

						<span class="s-text8 p-t-5 p-b-5">
							Showing 1–12 of 16 results
						</span>
					</div>
					-->

	<!-- Content page -->
					<!-- Product -->
					<div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                    <h3 style="margin-bottom:10px!important;"><b>Our Products</b></h3>
					<div class="row"> 
					<!-- {{$products}} -->
					@foreach($products as $product)
					<div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
							<!-- Block2 -->
							<div class="block2">
								<div class="block2-img wrap-pic-w of-hidden pos-relative"> <!--block2-labelnew-->
									
									<img src="/storage/images/{{$product->image}}">
									<div class="block2-overlay trans-0-4">
										<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
											<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
											<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
										</a>

										<div class="block2-btn-addcart w-size1 trans-0-4">
											<!-- Button -->
											<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
												<a  href="{{route('cart_items' , $product->id)}}" >Purchase </a>
											</button>
										</div>
									</div>
								</div>

								<div class="block2-txt p-t-20">
									
									<span class="block2-price p-r-5">
									{{$product->name}}
									</span>
										<br>
									<span class="block2-price  p-r-5">
									Quantity: {{$product->quantity}}
									</span>
										<br>
									<span class="block2-price  p-r-5">
								Price: $	{{$product->price}}
									</span>
								</div>
							</div>
						</div>
					@endforeach
					</div>

					<!-- Pagination -->
					<!-- <div class="pagination flex-m flex-w p-t-26">
						<a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
						<a href="#" class="item-pagination flex-c-m trans-0-4">2</a>
					</div> -->
				</div>
			</div>
		</div>
	</section>
@endsection