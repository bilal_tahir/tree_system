@extends('ecommerce_layout.main')
@section('title')
Carts
@endsection
@section('content')
<!-- Cart -->
	<section class="cart bgwhite p-t-70 p-b-100">
		<div class="container">
	<form>
  <div class="form-group">
    <label style="font-weight:bold">Name</label>
    <input type="text" class="form-control" name="u_name"  placeholder="Enter Your Name" style="border: 1px solid black !important; width:50%;">
  </div>
  <div class="form-group">
    <label style="font-weight:bold">Address</label>
    <input type="text" class="form-control" name="u_address"  placeholder="Enter your Address" style="border: 1px solid black !important; width:50%;">
  </div>
  <div class="form-group">
    <label style="font-weight:bold">Product Quantity</label>
    <input type="text" class="form-control" name="quantity" placeholder="Enter Quantity" style="border: 1px solid black !important; width:50%;">
  </div>
  <div class="form-group">
    <label style="font-weight:bold">Shipping Country</label>
    <input type="text" class="form-control" name="u_country" placeholder="Enter Your Country" style="border: 1px solid black !important; width:50%;">
    <small id="emailHelp" class="form-text text-muted">Shipping charges varies from country to country</small>
  </div>
  <div class="form-group">
    <label style="font-weight:bold">Shipping City</label>
    <input type="text" class="form-control" name="u_city" placeholder="Enter Your City" style="border: 1px solid black !important; width:50%;">
  </div>
  

  <button type="submit" class="btn btn-primary">Confirm Order</button>
</form>


		</div>
	</section>

@endsection