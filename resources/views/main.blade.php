
@extends('layouts.app')

@section('content')
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('asset/images/bg-01.jpg');">
			
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					<span class="login100-form-logo">
					<img src="{{asset('assets/img/logo.jpg')}}" style="height:-webkit-fill-available; width:-webkit-fill-available;"/>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						WelCome To Get29
					</span>
<label for="package" class="col-md-4 col-form-label text-md-right">{{ __('UserName') }}</label>
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="{{ Auth::user()->name }}">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
<label for="package" class="col-md-4 col-form-label text-md-right">{{ __('Refferals') }}</label>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="{{ Auth::user()->count }}">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
<label for="package" class="col-md-4 col-form-label text-md-right">{{ __('UserEmail') }}</label>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="{{ Auth::user()->email }}">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
<label for="package" class="col-md-4 col-form-label text-md-right">{{ __('Refferal Id') }}</label>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="{{ Auth::user()->id }} ">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<label for="package" class="col-md-4 col-form-label text-md-right">{{ __('Reward') }}</label>
                    <div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="{{ Auth::user()->reward }} ">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
<!-- 
					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div> -->

				

					<!-- <div class="text-center p-t-90">
						<a class="txt1" href="#">
							Forgot Password?
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

@endsection