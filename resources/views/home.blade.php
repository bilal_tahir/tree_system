@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <!-- <div class="card">
                 <div class="card-header">Dashboard</div>

                 <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div> 

        </div> -->

 <div id="eq-loader">
      <div class="eq-loader-div">
          <div class="eq-loading dual-loader mx-auto mb-5"></div>
      </div>
    </div>

    <form class="form-register" method="POST" action="{{url('/registered')}}" style="background:none" >
     @csrf
                     <meta name="csrf-token" content="{{ csrf_token() }}">
       

            <div class="row">
            <div class="col-md-12 text-center mb-4">
               <h3>Please Complete The Registeration Process To Start Earning</h3>
            </div>
            </div>


                <label for="userid" class="sr-only">Sponsor ID</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="icon-sponsorid"><i class="flaticon-user-5"></i> </span>
                    </div>
                    <input style="text-transform: uppercase" type="text" name="sponsorid" id="sponsorid" class="form-control" placeholder="Sponsor ID" aria-describedby="sponsorid" required >
                </div>


                <label for="inputEmail" class="sr-only">Email address</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="icon-inputEmail"><i class="flaticon-email-fill-2"></i> </span>
                    </div>
                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder=" {{ Auth::user()->email }}" value="  {{ Auth::user()->email }}" aria-describedby="inputEmail" required >
                </div>
                
               
                <label for="inputCountry" class="sr-only">Country Name</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="icon-inputcountry"><i class="flaticon-Country"></i> </span>
                    </div>
                    <input type="country" name="country" id="inputcountry" class="form-control" placeholder="Country Name" aria-describedby="inputCountry" required >
                </div>
                
                <label for="inputState" class="sr-only">State</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="icon-inputSate"><i class="flaticon-location-line"></i> </span>
                    </div>
                 <select type="text" name="state" id="inputState" class="form-control" placeholder="Select Package" aria-describedby="inputState" required >
        <option  style="color: black" value="Basic 3$ Package">Basic 3$ Package</option>
              				<option  style="color: black" value="Standard 6$ Package">Standard 6$ Package</option>
              				<option  style="color: black" value="Premium 30$ Package">Premium 30$ Package</option>   
                    </select></div>
             
                
              
                
               
               

                
                <button name="submit" type="submit" class="btn btn-lg btn-gradient-success btn-block btn-rounded mb-4 mt-5" type="submit">Register</button>
           
         
           
            </div>

        </div>
    </form>

<!-- <script type="text/javascript">
     //  $.ajaxSetup({
     //      headers: {
     //          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     //      }
     //  });

     function submitContact(e) {
         //  alert("test");
         //  e.preventDefault();
         var sponsorid = $("input[name=sponsorid]").val();
         var username = $("input[name=username]").val();
         var email = $("input[name=email]").val();
         var phone = $("input[name=phone]").val();
         var country = $("input[name=country]").val();
         var package = $("input[name=package]").val();
         var bank = $("input[name=bank]").val();
         var bankaccountname = $("input[name=bankaccountname]").val();
         var password = $("input[name=password]").val();
        
         console.log("input message text here", phone);
         $.ajax({
             type: 'POST',
             url: '/usertotal',
             data: {
                 sponsorid: sponsorid,
                 username: username,
                 email: email,
                  phone: phone,
                 country: country,
                 package: package,
                  bank: bank,
                 bankaccountname: bankaccountname,
                 password: password,
                 _token: "{{csrf_token()}}"
             },
             success: function(data) {
                 alert(data.success);
             },
             error: function(data) {
              var x= "  Error please try again later";
            document.getElementById("validate").innerHTML = x;
           
             }
         });
     };
 </script> -->
    





    </div>
</div>
@endsection
