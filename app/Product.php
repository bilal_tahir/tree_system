<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      protected $table = 'products';

   protected $fillable = [ 'name', 'quantity', 'price', 'image','category','subcategory','created_at', 'updated_at'];

    protected $guarded = ['updated_at', 'created_at'];
}
