<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   
    public function handle($request, Closure $next)
    {
        if (Auth::user())
        {
            if(Auth::user()->id == 1)
        {
            return $next($request);
        }
            elseif(Auth::user()->id == 2)
        {
            return $next($request);
        }
            elseif(Auth::user()->id == 3)
        {
            return $next($request);
        }
            elseif(Auth::user()->id == 4)
        {
            return $next($request);
        }
        else
        {
            return redirect('/user');
        }
        }
    }
}
