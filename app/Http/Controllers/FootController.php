<?php

namespace App\Http\Controllers;

use App\Foot;
use Illuminate\Http\Request;

class FootController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Foot  $foot
     * @return \Illuminate\Http\Response
     */
    public function show(Foot $foot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Foot  $foot
     * @return \Illuminate\Http\Response
     */
    public function edit(Foot $foot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Foot  $foot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Foot $foot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Foot  $foot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Foot $foot)
    {
        //
    }
}
