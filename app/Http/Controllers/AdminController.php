<?php

namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Content;
use App\Contact;
use App\Foot;
use App\Asset;


use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $data = User::where('package', 'Basic 3$ Package')->get();
         $data1 = User::where('package', 'Standard 6$ Package')->get();
          $data2 = User::where('package', 'Premium 30$ Package')->get();
          
        // dd($data2);
        // return view('admin.dashboard', compact('data'), compact('data1'), compact('data2'));
return view('admin.dashboard')->with('userdata', $data)
				->with('userdata1',$data1)
				->with('userdata2',$data2);
    }
     public function index1()
    {
        $data = User::where('package', 'Basic 3$ Package')->get();
        
          
        // dd($data2);
        // return view('admin.dashboard', compact('data'), compact('data1'), compact('data2'));
return view('admin.basic')->with('userdata', $data);
    }
     public function index2()
    {
       
         $data1 = User::where('package', 'Standard 6$ Package')->get();
       
          
return view('admin.standard')->with('userdata1',$data1);
			
    }
      public function index3()
    {
      
          $data2 = User::where('package', 'Premium 30$ Package')->get();
          
  
return view('admin.premium')->with('userdata2',$data2);
    }
         public function index4()
    {
      
          $data4 = Product::all();
          
  
return view('admin.products')->with('userdata2',$data4);
    }
    
            public function index5($id)
    {
      Product::where('id', $id)->delete();
         
   return redirect()->intended('/adminproducts');
    }
        
            public function index6()
    {
     
         
  return view('admin.addproduct');
    }



    public function index7(Request $data)
    {
     
       
 if ($data->has('image')){
       $filenamewithext = $data->image->getClientOriginalName();

       $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

       $extension = $data->image->getClientOriginalExtension();

       $filenametostore = $filename .'_'.time().'.'.$extension;
      //  $path = $data->image->storeAs('public/images',$filenametostore  );

       $data->image->move(public_path('assetsss/images'), $filenametostore);

            // Alert::success('Success', 'Photo updated Successfully');
            // return redirect('/admin/updatelogo');

            $con = new Product();
            $con->name= $data->name;
             $con->quantity= $data->quantity;
              $con->price= $data->price;
               $con->category= $data->category;
                $con->subcategory= $data->subcategory;
                 $con->image= $filenametostore;
                  $con->save();

                  return redirect()->intended('/adminproducts');
        }
        
        else{
            Alert::success('Failed', 'Photo not updated Successfully');
            return redirect('/admin/updatelogo');

    }
}
    
            public function index8($id)
    {
     $data = Product::where('id', $id)->first();
        
   return view('admin.edit')->with('data',$data);
    }

               public function index9(Request $data)
    {
       if ($data->has('image')){
       $filenamewithext = $data->image->getClientOriginalName();

       $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

       $extension = $data->image->getClientOriginalExtension();

       $filenametostore = $filename .'_'.time().'.'.$extension;
      //  $path = $data->image->storeAs('public/images',$filenametostore  );
       $data->image->move(public_path('assetsss/images'), $filenametostore);

        Product::where('id',$data->id)
            ->update([
              'name' => $data->name,
              'quantity' => $data->quantity,
              'price' => $data->price ,
              'category' =>$data->category,
              'subcategory' =>$data->subcategory,
              'image' => $filenametostore 
              ]);
              return redirect()->intended('/adminproducts');
       }
else{
        Product::where('id',$data->id)
            ->update([
              'name' => $data->name,
              'quantity' => $data->quantity,
              'price' => $data->price ,
              'category' =>$data->category,
              'subcategory' =>$data->subcategory
              ]);
            return redirect()->intended('/adminproducts');
            }
  //    $data = Product::where('id', $id)->first();
        
  //  return view('admin.edit')->with('data',$data);
    }


    public function home()
    {
        $homecontent = Content::where('page','home')->get();
        return view ('admin.adminhome')->with('homecontent',$homecontent);
    }

    public function about()
    {
        $aboutcontent = Content::where('page','about')->get();
        return view ('admin.adminabout')->with('aboutcontent',$aboutcontent);
    }

    public function how()
    {
        $howcontent = Content::where('page','how')->get();
        return view ('admin.adminhow')->with('howcontent',$howcontent);
    }

    public function privacy()
    {
        $privacycontent = Content::where('page','privacy')->get();
        return view ('admin.adminprivacy')->with('privacycontent',$privacycontent);
    }

    public function refund()
    {
        $refundcontent = Content::where('page','refund')->get();
        return view ('admin.adminrefund')->with('refundcontent',$refundcontent);
    }

    public function contact()
    {
        $contactcontent = Content::where('page','contact')->get();
        return view ('admin.admincontact')->with('contactcontent',$contactcontent);
    }
    public function updatecontent($id)
    {
      $update = Content::where('id',$id)->first();
      return view ('admin.updatecontent')->with('update',$update);
    }

    public function update(Request $data)
    {

     Content::where('id',$data->id)
            ->update([
              'heading' => $data->heading,
              'text' => $data->content
      
              ]);
            return redirect()->intended('/admin');


    }


    public function submitcontact(Request $request)
    {
      $con = new Contact();
      $con->name=$request->fullname;
      $con->email=$request->email;
      $con->phone=$request->phone;
      $con->message=$request->message;

      $con->save();

      return redirect()->intended('/contactus')->withSuccess('Great! We will get back to you soon.');
    }

    public function message()
    {
      $messages = Contact::all();
      return view ('admin.message')->with('messages',$messages);
    }


    public function deletemessage($id)
    {
          Contact::where('id',$id)->delete();
          return redirect()->intended('/admin');
    }

    
    public function footer()
    {
         $footercon =  Foot::all();
        return view ('admin.adminfooter')->with('footercon',$footercon);
    }
    

    public function updatefooter($id)
    {
    
       $updatefoot = Foot::where('id',$id)->first();
      return view ('admin.updatefooter')->with('updatefoot',$updatefoot);
         
    }
    
     public function footerupdated(Request $data)
    {

     Foot::where('id',$data->id)
            ->update([
              'heading' => $data->heading,
              'text' => $data->text
      
              ]);
            return redirect()->intended('/admin');


    }
 
    public function paymentimages()
    {
         $images =  Asset::all();
        return view ('admin.paymentimages')->with('images',$images);
    }

    
    public function  updatepaymentimages($id)
    {
    
       $payimage = Asset::where('id',$id)->first();
       
      return view ('admin.updatepaymentimages')->with('payimage',$payimage);
         
    }

     public function paymentimageupdated(Request $data)
    {

        if ($data->has('image')){
       $filenamewithext = $data->image->getClientOriginalName();

       $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

       $extension = $data->image->getClientOriginalExtension();

       $filenametostore = $filename .'_'.time().'.'.$extension;
      //  $path = $data->image->storeAs('public/images',$filenametostore  );

      //  $path = $message->image->storeAs('public/images',$filenametostore  );
      $data->image->move(public_path('assetsss/images'), $filenametostore);


        Asset::where('id',$data->id)
            ->update([
              'image' => $filenametostore 
              ]);
              return redirect()->intended('/admin');
       }


    }
  
     public function   addpaymentimages()
    {
        
        return view ('admin.addpaymentimages');
    }

   public function paymentimageadded(Request $data)
    {

        if ($data->has('image')){
       $filenamewithext = $data->image->getClientOriginalName();

       $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);

       $extension = $data->image->getClientOriginalExtension();

       $filenametostore = $filename .'_'.time().'.'.$extension;
      //  $path = $data->image->storeAs('public/images',$filenametostore  );

      //  $path = $message->image->storeAs('public/images',$filenametostore  );
      $data->image->move(public_path('assetsss/images'), $filenametostore);

      $asset = new Asset();
      $asset->image = $filenametostore ;
      $asset->save();
      return redirect()->intended('/admin');
       }


    }


    public function deleteimage()
    {
      
      $payimg = Asset::all();
         return view ('admin.deletepaymentimages')->with('payimg',$payimg);
    }
    
     public function imagedeleted($id)
    {
          Asset::where('id',$id)->delete();
          return redirect()->intended('/admin');
    }
    
}
