<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
     protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            // 'sponsorid' => ['required', 'integer', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
             'number' => ['required', 'string','max:255', 'unique:users'],
            //  'country' => ['required', 'string', 'max:255'],
            //  'package' => ['required', 'string', 'max:255'],
             'bankname' => ['required', 'string', 'max:255'],
            //  'bankaccountname' => ['required', 'string', 'max:255'],
             'bankaccountnumber' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

    //     dd($data['country']
    // );        
        return User::create([

            'name' => $data['name'],
            'email' => $data['email'],
            'p_id' => '0',
            'm_id' => '0',
            'count' => 0,
            'number' => $data['number'],
            'package' => 'not selected',
            'bank' => $data['bankname'],
            'account_name' => 'not required',
            'account_number' => $data['bankaccountnumber'],
            'ip_address' => request()->ip(),
            'reward' => '0',
            'password' => Hash::make($data['password']),
        ]);



// //IF for checking refferal user or not

// if($message['sponsorid'] != 0 )
// {

// // If part for checking user max 2 childs
// $count = User::where('p_id',$message['sponsorid'])->count();
// if($count<2)
// {



// $count5 = User::where('id',$message['sponsorid'])->first();
// $count6 = $count5->m_id;
// $g = 0;
// $result7 = explode('/',$count6);
// foreach ($result7 as $f)
// {
//     $g = $g+1;
// }


// //Here putting logic for count = 6
// $re = User::where('id',$message['sponsorid'])->first();
// $rer = $re->m_id;
// $ffg = explode('/',$rer);
// $fty = $ffg[0];
// $frt = User::where('id',$fty)->first();
// $tyu = $frt->count;
// if($tyu >= 6)
// {
//     $g = 2;
// }


// if($g < 3) //beacuse 2 level testing
// {
// //Code for count
// $count1 = User::where('id',$message['sponsorid'])->first();
// $count2 = $count1->m_id;
// $result1 = explode('/',$count2);
// foreach ($result1 as $i)
// {
//     $first = User::where('id',$i)->first();
//     $second = $first->count;
//     $third = $second + 1;
//     User::where('id', $i)
//     ->update(['count' =>$third ]);
// }

// $check_package = User::where('id',$message['sponsorid'])->first();

// if($message['package'] == $check_package->package)
// {
// //Adding into database
// $register = new User();
// $register->p_id=$message['sponsorid'];
// $register->name=$message['name'];
// $register->email=$message['email'];
// $register->number=$message['number'];
// $register->country=$message['country'];      
// $register->package=$message['package'];           
// $register->bank=$message['bank'];
// $register->account_name=$message['bankaccountname'];
// $register->account_number=$message['bankaccountnumber'];
// $register->password=$message['password'];
// $register->ip_address=request()->ip();
// $register->reward=0;
// $register->count=0;
// $register->m_id='9';
// $register->save();

// $data=User::where('m_id','9')->first();
// $data1=User::where('id',$message['sponsorid'])->first();
// User::where('m_id', '9')
// ->update(['m_id' =>$data1->m_id.'/'.$data->id ]);


// $data=User::where('email',$register->email)->first();
// return $data;
// }
// else{ //else part
//    return back()->withInput()->with('error', 'There was an error...');
// }




// $del = substr_count($data1->m_id.'/'.$data->id,"/");
// if($del >= 2)
// {
// $input = $data1->m_id.'/'.$data->id;
// $result = explode('/',$input);


// // Code for checking count
// $check = $result[0];

//  $data23=User::where('id',$check)->first();
 
// if ($data23->count >= 7)

// {
// $ddi = $result[1].'/'.$result[2].'/'.$result[3];  // .'/'.$data->id;

// User::where('m_id', $data1->m_id.'/'.$data->id)
// ->update(['m_id' =>$ddi]);

// }

// }











// //Checking New USer Added

// if ($register) {
//         //  echo "<script>alert('Registered');</script>";
//     } else {
//        return back()->withInput()->with('error', 'There was an error...');
//     }

// } //Checking level  ended
// else{
//    return back()->withInput()->with('error', 'There was an error...');
// }

// } // else part of count id
// else {
//   return back()->withInput()->with('error', 'There was an error...');
//     }

// } //If ending of refferal user checking
// else{
// $register = new User();
// $register->p_id=0;
// $register->name=$message['name'];
// $register->email=$message['email'];
// $register->number=$message['number'];
// $register->country=$message['country'];      
// $register->package=$message['package'];           
// $register->bank=$message['bank'];
// $register->account_name=$message['bankaccountname'];
// $register->account_number=$message['bankaccountnumber'];
// $register->password=$message['password'];
// $register->ip_address=request()->ip();
// $register->reward=0;
// $register->count=0;
// $register->m_id='9';
// $register->save();
    

// $data3=User::where('m_id','9')->first();
// User::where('m_id', '9')
// ->update(['m_id' =>$data3->id ]);

// $data=User::where('email',$register->email)->first();
// return $data;
// }


    }
}
