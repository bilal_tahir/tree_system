<?php

namespace App\Http\Controllers;
use App\Product;
use App\Carts;
use App\Foot;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
            

    }
         public function toyota()
    {
         $products = Product::where('category','toyota')->get();
          $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }


     public function honda()
    {
        $products = Product::where('category','honda')->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

     public function suzuki()
    {
        $products = Product::where('category','suzuki')->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

     public function hyundai()
    {  
        $products = Product::where('category','hyundai')->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }


     public function nissan()
    {
    
        $products = Product::where('category','nissan')->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }


    // Section start for product filters
      public function keyword()
    {
        $products = Product::where('category','nissan')->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

      public function price1()
    {   
        $products = Product::where('price','<', 51)->get();
        $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

      public function price2()
    {
          $products = Product::whereBetween('price',[51,100])->get();
          $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

      public function price3()
    {
          $products = Product::whereBetween('price',[101,150])->get();
          $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

      public function price4()
    {
          $products = Product::whereBetween('price',[151,200])->get();
          $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }

      public function price5()
    {
          $products = Product::where('price','>','200')->get();
          $footercontent = Foot::all();
         return view('ecommerce.main',compact('products','footercontent'));
    }
          public function cart($cart)
    {


      
          $cart_item = new Product();
          $cart_item = $cart_item->where('id','=',$cart)->first();
          $footercontent = Foot::all();
         return view('ecommerce.purchase',compact('cart_item','footercontent'));

          // $storeCart = new Carts();
          // $storeCart->user_id = $cart_item->id;
          // $storeCart->name = $cart_item->name;
          // $storeCart->price = $cart_item->price;
          // $storeCart->image = $cart_item->image;
          // $storeCart->quantity = $cart_item->quantity;
          // $storeCart->category = $cart_item->category;
          // $storeCart->subcategory=  $cart_item->subcategory;

          // $storeCart->save();
          // return redirect()->intended('products');
    }

     public function showcart()
    {
        $carts = Carts::all();
         $footercontent = Foot::all();
         return view('ecommerce.purchase',compact('carts','footercontent'));
     
    }
       public function cartdelete($cart)
    {
      
            $deletedRows = Carts::where('id','=', $cart)->delete();
            return redirect()->intended('carts');
    }

}
