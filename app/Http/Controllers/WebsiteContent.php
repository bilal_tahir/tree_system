<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Foot;
use App\Asset;

class WebsiteContent extends Controller
{
    public function home()
    {
        $homecontent = Content::where('page','home')->get();
        $footercontent = Foot::all();
        $paymentimages = Asset::all();

         return view('website.home',compact('homecontent','footercontent','paymentimages'));
     
    }

    public function about()
    {
        $aboutcontent = Content::where('page','about')->get();
         $footercontent = Foot::all();
         return view('website.about',compact('aboutcontent','footercontent'));
        
    }

    public function how()
    {
        $howcontent = Content::where('page','how')->get();
            $footercontent = Foot::all();
         return view('website.packages',compact('howcontent','footercontent'));
        
    }

    public function privacy()
    {
        $privacycontent = Content::where('page','privacy')->get();
            $footercontent = Foot::all();
         return view('website.privacypolicy',compact('privacycontent','footercontent'));
       
    }

    public function refund()
    {
        $refundcontent = Content::where('page','refund')->get();
        $footercontent = Foot::all();
         return view('website.refundpolicy',compact('refundcontent','footercontent'));
      
    }

    public function contact()
    {
        $contactcontent = Content::where('page','contact')->get();
        $footercontent = Foot::all();
         return view('website.contact',compact('contactcontent','footercontent'));
      
    }
    
}

