<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{
    protected $table = 'carts';

    protected $fillable = [ 'user_id','name', 'quantity', 'price', 'image','category','subcategory','created_at', 'updated_at'];

    protected $guarded = ['updated_at', 'created_at'];
}
