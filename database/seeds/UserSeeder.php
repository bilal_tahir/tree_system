<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'id' => 1,
             'name' => "Administrator",
             'email' => "tasha1122@admin.com",
             'p_id' => 0,
             'm_id' => "0",
             'count' => 0,
             'number' => "***********",
             'country' => "Nigeria",
             'package' => "Not Defined",
             'bank' => "Not Defined",
             'account_name' => "Not Defined",
             'account_number' => "Not Defined",
             'ip_address' => "Not Defined",
             'reward' => 0,
             'password' => bcrypt("tasha@admin9080;"),
             
         ]);
           DB::table('users')->insert([
            'id' => 2,
             'name' => "Administrator",
             'email' => "tasha1133@admin.com",
             'p_id' => 0,
             'm_id' => "0",
             'count' => 0,
             'number' => "**********",
             'country' => "Nigeria",
             'package' => "Not Defined",
             'bank' => "Not Defined",
             'account_name' => "Not Defined",
             'account_number' => "Not Defined",
             'ip_address' => "Not Defined",
             'reward' => 0,
             'password' => bcrypt("tasha@admin0879;"),
             
         ]);
           DB::table('users')->insert([
            'id' => 3,
             'name' => "Administrator",
             'email' => "tasha1144@admin.com",
             'p_id' => 0,
             'm_id' => "0",
             'count' => 0,
             'number' => "*********",
             'country' => "Nigeria",
             'package' => "Not Defined",
             'bank' => "Not Defined",
             'account_name' => "Not Defined",
             'account_number' => "Not Defined",
             'ip_address' => "Not Defined",
             'reward' => 0,
             'password' => bcrypt("tasha@admin3646;"),
             
         ]);
           DB::table('users')->insert([
            'id' => 4,
             'name' => "Administrator",
             'email' => "tasha1155@admin.com",
             'p_id' => 0,
             'm_id' => "0",
             'count' => 0,
             'number' => "********",
             'country' => "Nigeria",
             'package' => "Not Defined",
             'bank' => "Not Defined",
             'account_name' => "Not Defined",
             'account_number' => "Not Defined",
             'ip_address' => "Not Defined",
             'reward' => 0,
             'password' => bcrypt("tasha@admin6723;"),
             
         ]);
    }
}
