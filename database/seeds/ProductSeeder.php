<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('products')->insert([
            'id' => 1,
             'name' => "Product1",
             'quantity' => 12,
             'price' => 45.00,
             'image' => "p_6_1582103488.png",
             'category' => "Hyundai",
             'subcategory' => "Brake Leather",
             
         ]);

          DB::table('products')->insert([
            'id' => 2,
             'name' => "Product2",
             'quantity' => 124,
             'price' => 434.00,
             'image' => "p_5_1582105477.png",
             'category' => "Toyota",
             'subcategory' => "Engine Plug",
             
         ]);

        
    }
}
