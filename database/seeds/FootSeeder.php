<?php

use Illuminate\Database\Seeder;

class FootSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('feet')->insert([
            'id' => 1,
             'heading' => "OUR MISSION",
             'text' => "We are on a mission to empower people’s lives in the area of charity, economy, finance and infrastructure.",
         ]);

          DB::table('feet')->insert([
            'id' => 2,
             'heading' => "CAN I HAVE MORE THAN ONE ACCOUNT?",
             'text' => "Yes, you can have more than one account and investment with us. As long as you can introduce at least 2 active persons to the network, you are good to go.",
         ]);

    }
}
