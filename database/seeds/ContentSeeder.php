<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('contents')->insert([
            'id' => 1,
             'page' => "home",
             'heading' => "Basic 3$ Package",
             'text' => " <p>Basic 3$ package that rewards 7.5$ after successfull completion of childs.</p>",     
         ]);

          DB::table('contents')->insert([
            'id' => 2,
             'page' => "home",
             'heading' => "Standard 6$ Package",
             'text' => "<p>Standard 6$ package that rewards 12.5$ after successfull completion of childs.</p>",     
         ]);

          DB::table('contents')->insert([
            'id' => 3,
             'page' => "home",
             'heading' => "Premium 30$ Package",
             'text' => "<p>Premium 30$ package that rewards 61.5$ after successfull completion of childs.</p>",     
         ]);

          DB::table('contents')->insert([
            'id' => 4,
             'page' => "about",
             'heading' => "What is Get29 all about?",
             'text' => "<p>Get29 is first of its kind to operate with uncrushable fast-rewarding matrix in the Multi-Level Marketing (MLM) Network all across the world. Operating MLM investments with a token of $3, $6 or $30ismade possible by Get29 systems worldwide.</p>  ",     
         ]);

          DB::table('contents')->insert([
            'id' => 5,
             'page' => "about",
             'heading' => "Our Mission",
             'text' => "<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>"
              ]);

          DB::table('contents')->insert([
            'id' => 6,
             'page' => "how",
             'heading' => "How it works",
             'text' => "<p>We use the compensation plan 1x2 matrix to reward our investors. Our compensation plan is simply the best (Note that “A” means “You”).
When “A” registers and invests $6. All “A” needs to do is to refer 2 persons (B and C), then“A” gets 50% ($3) of his investment capital as first compensation (which is level 1 completion). For “A” to get second compensation (100% of his investment capital “$6” + bonus), “B” and “C” must have gotten 2 persons each as downline (Which is Level Two completion).  “A” continues to get 100% of his investment capital + bonus at the completion of each level until it reaches level 9, where “A” decides whether or not to remain in his current investement plan or upgrade to another plan. 
Note: “A” can earn his normal compensations as stated above without referring 2 persons at level 1. This is possible because reaching compensation stage at any level is easily achieved through collective efforts of all investors in the chain. If the sponsors before “A” would refer other people to the network, they will be added under “A”. So, “A” possibly gets compensated without any effort. Though, for maximum results in this networking system, we would suggest all hands to the pumps.
</p> ",     
         ]);

          DB::table('contents')->insert([
            'id' => 7,
             'page' => "privacy",
             'heading' => "Privacy Policy",
             'text' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>",     
         ]);

          DB::table('contents')->insert([
            'id' => 8,
             'page' => "refund",
             'heading' => "Refund Policy",
             'text' => "<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>",     
         ]);

          DB::table('contents')->insert([
            'id' => 9,
             'page' => "contact",
             'heading' => "Address",
             'text' => "<p>203 Fake St. Mountain View, San Francisco, California, USA</p>",     
         ]);

           DB::table('contents')->insert([
            'id' => 10,
             'page' => "contact",
             'heading' => "Phone",
             'text' => "<p>+1 232 3235 324</p>",     
         ]);

           DB::table('contents')->insert([
            'id' => 11,
             'page' => "contact",
             'heading' => "Email Address",
             'text' => "<p>youremail@domain.com</p>",     
         ]);
    }
}
