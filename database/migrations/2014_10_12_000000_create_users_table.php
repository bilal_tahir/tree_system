<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
              $table->bigInteger('p_id');
             $table->String('m_id');
             $table->bigInteger('count');
              $table->string('number')->unique();
                 $table->string('country')->nullable();
                    $table->string('package');
                    $table->string('bank');
          $table->string('account_name');
          $table->string('account_number');
             $table->string('ip_address');
             $table->bigInteger('reward');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
